import { Company } from "@/domain/entities";

export class CompanyViewModel {
  _id: string;
  name: string;
  active: boolean;
  createdAt: string;
  updatedAt: string;

  static map(entity: Company): CompanyViewModel {
    return {
      ...entity,
      createdAt: entity.createdAt.toISOString(),
      updatedAt: entity.updatedAt.toISOString(),
    };
  }

  static mapCollection (entities: Company[]): CompanyViewModel[] {
    return entities.map(CompanyViewModel.map)
  }
}
