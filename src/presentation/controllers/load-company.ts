import { CompanyLoader } from "@/domain/usecases";
import { Controller, HttpResponse, serverError, ok } from "@/presentation/protocols";
import { CompanyViewModel } from "@/presentation/view-models";

export class LoadCompanyController implements Controller {
  constructor(private readonly companyLoader: CompanyLoader){}
  async handle(): Promise<HttpResponse<CompanyViewModel[]>>{
    try {
      const company = await this.companyLoader.load()
      console.log({company})
      return ok(CompanyViewModel.mapCollection(company))
    } catch {
      return serverError(new Error)
    }
  }
}