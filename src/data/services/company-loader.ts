import { Company } from "@/domain/entities";
import { CompanyLoader } from "@/domain/usecases";
import { CompanyLoaderRepository } from '@/data/protocols'

export class CompanyLoaderService implements CompanyLoader {
  constructor(private readonly companyLoaderRepository: CompanyLoaderRepository ){}
  async load(): Promise<Company[]>{
    return this.companyLoaderRepository.companyLoader()
  }
}