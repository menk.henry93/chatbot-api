import { CompanyModel } from "@/data/models";

export interface CompanyLoaderRepository {
  companyLoader: () => Promise<CompanyModel[]>
}