import { Controller } from "@/presentation/protocols";
import { CompanyLoaderService } from '@/data/services'
import { LoadCompanyController } from '@/presentation/controllers'
import { MongodbCompanyRepository } from "@/infra/db/mongodb/repositories";

export const makeLoadCompanyController = (): Controller => {
  const repository = new MongodbCompanyRepository()
  const loader = new CompanyLoaderService(repository)
  return new LoadCompanyController(loader)
}