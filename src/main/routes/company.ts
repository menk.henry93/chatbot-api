import { adaptRouter } from '@/main/adapters'
import { makeLoadCompanyController } from '@/main/factories'

import { Router } from 'express'

export default (router: Router): void => {
  router.get('/companies', adaptRouter(makeLoadCompanyController()))
}
