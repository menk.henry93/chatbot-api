import 'module-alias/register'
import app from '@/main/config/app'

app.listen(process.env.PORT, () =>
  console.log(`Server running at http://localhost:${process.env.PORT}`)
)