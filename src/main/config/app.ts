import dataBaseConnetion from '@/infra/db/mongodb/connectionDatabase'
import { setupRoutes } from '@/main/config/routes'

import * as dotenv from 'dotenv'
import express from 'express'

dotenv.config()
const app = express()
setupRoutes(app)
export const db = dataBaseConnetion();
export default app
