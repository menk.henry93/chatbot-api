export const company = [
  {
    _id: '1',
    name: 'Condomínio Alvoredo',
    phone_number: '(44)997711456',
    active: true,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    _id: '2',
    name: 'Condomínio Santa Monica',
    phone_number: '(44)987511456',
    active: true,
    createdAt: new Date(),
    updatedAt: new Date()
  }
]