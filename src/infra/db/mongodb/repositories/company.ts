import { CompanyModel } from '@/data/models'
import { CompanyLoaderRepository } from '@/data/protocols'
import CompanyMongodbModel from '../entities/company'

export class MongodbCompanyRepository implements CompanyLoaderRepository {
  async companyLoader(): Promise<CompanyModel[]> {
    const result = CompanyMongodbModel.find()
    return result
  }
}