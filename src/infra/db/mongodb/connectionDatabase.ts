import 'dotenv/config'
import mongoose from 'mongoose'
let dataBase: any

export default function dataBaseConnetion(): mongoose.Connection {
  if (!dataBase) {
    dataBase = mongoose
      .createConnection(<string>process.env.MONGO_URI,)
      .on('connected', (): void => {
        console.log("Mongodb conectado")
      })
  }

  return dataBase
}
