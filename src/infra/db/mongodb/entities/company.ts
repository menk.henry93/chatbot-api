import { Company } from '@/domain/entities'
import { Schema } from 'mongoose'
import v4 from 'uuid'
import dataBaseConnetion from './../connectionDatabase'

const CompanySchema = new Schema(
  {
    _id: {
      type: String,
      default: v4,
    },
    name: { type: String },
    phone_number: { type: String },
    active: { type: Boolean },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
  }
)

const db = dataBaseConnetion()
export default db.model<Company>('companys', CompanySchema)
