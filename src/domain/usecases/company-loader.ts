import { Company } from "@/domain/entities";

export interface CompanyLoader {
  load: () => Promise<Company[]>
}