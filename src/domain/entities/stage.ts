export type Stage = {
  _id: string
  step: string
  description: string
  company_id: string
  active: boolean
  createdAt: Date
  updatedAt: Date
}