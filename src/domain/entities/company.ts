export type Company = {
  _id: string
  name: string
  phone_number: string
  active: boolean
  createdAt: Date
  updatedAt: Date
}